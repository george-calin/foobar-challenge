# just constants
MAX_NO_ROBOTS = 42
SPEED = 50
DEBUG = False

MINE_FOO = "MINE_FOO"
MINE_BAR = "MINE_BAR"
MINE_FOOBAR = "MINE_FOOBAR"
SELL = "SELL"
BUY = "BUY"
ACTIONS = [MINE_FOO, MINE_BAR, MINE_FOOBAR, SELL, BUY] 

# i got these numbers with some quick and not so good math 
MAX_PROCENTAGES = {
    MINE_FOO: 0.20,
    MINE_BAR: 0.14,
    MINE_FOOBAR: 0.30,
    SELL: 0.34,
    BUY: 0.1
}

ACTION_DURATIONS = {
    MINE_FOO: 1,
    MINE_BAR: 0,
    MINE_FOOBAR: 2,
    SELL: 10,
    BUY: 1
}

CHANGE_ACTIVITY_DURATION = 20













