# Challenge 'Foobar Factory'

## The challenge

We start with two robots ; each can do several actions but only one at a time:

* Change activity: the robot is busy 20 seconds. 
* `foo` mining: the robot is busy 1 second
* `bar` mining: the robot is busy a random time between 0.5 and  2 seconds 
* `foobar` assembly : the robot is busy 2 seconds . It takes a `foo` and a `bar` and creates a `foobar`
This operation has 70% succes rate . If it fails you get back the `bar` , but you lose the `foo`.

* Sell some `foobar`: 10 seconds to sell `foobar`s between 2 to 4 (random) foobars , we get 1 RON for each `foobar` sold.
* Buy new robot : The robot is busy 1 second . The new robot costs 3 RON's and 2 `foo`'s

The game stops when you reach 42 robots !


If you want to custom the run, change these constants from Utils.py: 
- MAX_NO_ROBOTS = 42
- SPEED = 50
- DEBUG = False

