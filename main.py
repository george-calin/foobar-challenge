from utils import *
import datetime
import random
import threading
import time

class Pipeline:
    def __init__(self):
        # current time 
        self.time_now = 0

        # similiar(ish) to javascript event queue
        # timeline array contains events are scheduled to finish at a certain timestamp
        # timeline is in ascending order by timestamp
        self.timeline = []

        # events_hanging array contains events that could not be scheduled 
        # because of the lack of materials
        # when there are enough materials for an event, the event pops from events_hanging into the timeline queue 
        self.events_hanging = []


        # resources available 
        self.store = {
            'noBars': 0,
            'noFoos': 0,
            'noFoobars': 0,
            'budget': 0,
            'noRobots': 2
        }

        # dynamic assigning handlers based on event action
        self.reducer_schedule = {
            MINE_FOO: self.schedule_mine_foo,
            MINE_BAR: self.schedule_mine_bar,
            MINE_FOOBAR: self.schedule_assembly_foobar,
            SELL: self.schedule_sell_foobar,
            BUY: self.schedule_buy_robot,
        }

        # dictionary holding track of number of robots by activity/action
        self.reducer_noRobots = {
            MINE_FOO: 1,
            MINE_BAR: 1,
            MINE_FOOBAR: 0,
            SELL: 0,
            BUY: 0
        }

        # hardcoded logic ( and not optimized logic ) for the initial phase of the program
        # until the pipeline has enough robots to be fully autonomous
        # check the check_checkpoints function description for more info
        self.aCheckpoints = [False, False, False, False, False]
        # flag if all the checkpoints are done
        self.checkpoints_done = False

        # start the process
        self.start_pipeline()

    def start_pipeline(self):
        # hardcode the first 2 events
        self.dispatch(event={
            "timestamp": 1,
            "action": MINE_FOO,
            "change_activity": False
        })

        self.dispatch(event={
            "timestamp": 0,
            "action": MINE_BAR,
            "change_activity": False
        })

        time_last_event = 0
        event = None
        while True:
            # deal with hanging events
            self.deal_hanging_events()
            # get the next finished event
            try:
                event = self.timeline.pop(0)
            except IndexError:
                # timeline is empty, the program is left hanging
                print("!!! DEADLOCK !!!")
                exit()

            # cash in on the current event ( get new materials/money/robots )
            self.cash_in(event)

            if DEBUG:
                print(self.store) 

            # change the timestamp to the current event
            self.time_now = event["timestamp"]
            
            # set the ilusion of time passing
            time_to_sleep = max(self.time_now - time_last_event, 0)
            time.sleep(time_to_sleep / SPEED)
            # keep the current event timestamp to compare with the next event
            time_last_event = event["timestamp"]

            # if the program did not finish the initial checkpoints
            if not self.checkpoints_done:
                event = self.check_checkpoints(event)

            # if the event can be done again, schedule it in the timeline
            if self.can_be_done(event):
                self.dispatch(event) 
            # else put it in the waiting queue
            else:
                self.events_hanging.append(event)

 
    # dispatches an event to the timeline through different functions based on the action of the event 
    def dispatch(self, event):
        action = event["action"]
        if DEBUG:
            self.log_action(event)
        if event["change_activity"]:
            self.reducer_noRobots[action] += 1
        # schedule the event based on the action
        self.reducer_schedule[action](event, ACTION_DURATIONS[action])

    # debug function
    # print usefull information
    def log_action(self, event):
        print("Seconds passed: {0} | Robot {1}".format(self.time_now, event))
        print("Robot distribution: {}".format(self.reducer_noRobots))

    # 
    def schedule_mine_foo(self, event, duration):
        self.schedule_event(event=event, duration=duration)
    # 
    def schedule_mine_bar(self, event, duration): 
        duration = random.uniform(0.5, 2)
        self.schedule_event(event=event, duration=duration)
    # 
    def schedule_assembly_foobar(self, event, duration):
        self.store["noFoos"] -= 1
        self.store["noBars"] -= 1
        self.schedule_event(event=event, duration=duration)
    # 
    def schedule_sell_foobar(self, event, duration):
        units_sold = random.randint(2, min(4, self.store["noFoobars"]))

        self.store["noFoobars"] -= units_sold
        event["units_sold"] = units_sold
        self.schedule_event(event=event, duration=duration)
    #  
    def schedule_buy_robot(self, event, duration):
        self.store["budget"] -= 3
        self.store["noFoos"] -= 2
        self.schedule_event(event=event, duration=duration)

    # handler for inserting an event (finished activity) in the timeline, based on the time it will be finished 
    # (reminder: the timeline is always in ascending order by timestamp)
    def schedule_event(self, event, duration):
        event["timestamp"] = self.time_now + duration
        if event["change_activity"]:
            event["change_activity"] = False
            event["timestamp"] += CHANGE_ACTIVITY_DURATION

        insert_index = self.find_index(event["timestamp"])

        self.timeline.insert(insert_index, event)

    # finds and returns the index at which the event should be inserted
    def find_index(self, searched_time):
        for idx, item  in enumerate(self.timeline):
            if searched_time <= item["timestamp"]:
                return idx
        # if not found, it's the first
        return len(self.timeline)

    # checks if the store has enough materials for an event/action
    def can_be_done(self, event):
        # renaming
        action = event['action']

        if action == MINE_FOO or action == MINE_BAR:
            return True
        if action == MINE_FOOBAR:
            return self.store["noBars"] >= 1 and self.store["noFoos"] >= 1
        if action == SELL:
            return self.store["noFoobars"] >= 2
        if action == BUY:
            return self.store["budget"] >= 3 and self.store["noFoos"] >= 2
        
        return False

    # handler for dealing with the hanging/pending/delayed events ( postponed because they could not be done because of lack of materials )
    def deal_hanging_events(self):
        events_hanging = self.events_hanging.copy()
        for idx, event in enumerate(events_hanging):
            if self.can_be_done(event):
                self.events_hanging.pop(idx)
                self.dispatch(event)

    # Hardcoded logic, suggest you read only the description
    # moment 0 -> robot 1 mines foo, robot 2 mines bar untill we got 15 bars
    # checkpoint 0 -> robot 2 changes activity from bar to foobar, and mines it untill we got 15 foobars 

    # checkpoint 1 -> robot 2 changes activity from foobar to selling untill there are no more foobars
    # checkpoint 2 -> robot 1 changes activity from foo to bar, 

    # checkpoint 3 -> robot 2 changes activity from selling to foobar
    # checkpoint 4 -> robot 1 changes activity from bar to buying robots 

    # checkpoints_done => the pipeline is fully autonomous 
    def check_checkpoints(self, event):
        # if the first checkpoint conditions are met
        if  (   
                event["action"] == MINE_BAR and \
                not self.aCheckpoints[0] and \
                self.store["noBars"] >= 15
            ):
            self.aCheckpoints[0] = True
            event["action"] = MINE_FOOBAR
            event["change_activity"] = True
            self.reducer_noRobots[MINE_BAR] -= 1
        # if the second checkpoint conditions are met
        elif (
                self.store["noFoobars"] >= 15 and \
                self.aCheckpoints[0] and \
                not self.aCheckpoints[1] and \
                event["action"] == MINE_FOOBAR
            ):

            self.aCheckpoints[1] = True
            event["action"] = SELL
            event["change_activity"] = True
            self.reducer_noRobots[MINE_FOOBAR] -= 1
        # if the third checkpoint conditions are met
        elif (
                self.aCheckpoints[1] and \
                not self.aCheckpoints[2]  and \
                event["action"] == MINE_FOO
            ):
            self.aCheckpoints[2] = True
            event["action"] = MINE_BAR
            event["change_activity"] = True
            self.reducer_noRobots[MINE_FOO] -= 1 
        # if the forth checkpoint conditions are met
        elif (
                self.aCheckpoints[2] and \
                not self.aCheckpoints[3] and \
                self.store["noFoobars"] < 2 and \
                event["action"] == SELL      
            ):
            self.aCheckpoints[3] = True
            event["action"] = MINE_FOOBAR
            event["change_activity"] = True
            self.reducer_noRobots[SELL] -= 1 
        # if the fifth checkpoint conditions are met
        elif (
                self.aCheckpoints[3] and \
                not self.aCheckpoints[4] and \
                event["action"] == MINE_BAR     
            ):
            self.aCheckpoints[4] = True
            event["action"] = BUY
            event["change_activity"] = True
            self.reducer_noRobots[MINE_BAR] -= 1
            self.checkpoints_done = True

        return event

    # cash in on an event
    def cash_in(self, event):
        action = event["action"] 
        if action == MINE_FOO:
            self.store["noFoos"] += 1

        elif action == MINE_BAR:
            self.store["noBars"] += 1

        elif action == MINE_FOOBAR:
            success = random.uniform(0, 1) < 0.7
            if success:
                self.store["noFoobars"] += 1
            else:
                # get back the bar
                self.store["noBars"] += 1

        elif action == SELL:
            #  number of units sold 
            units_sold = event["units_sold"]
            self.store["budget"] += units_sold

        elif action == BUY:
            self.buy_robot()

    # handle for buying the robot, and assigning him a task
    def buy_robot(self):
        # choose the activity for the new robot and create and event for him 
        new_robot_action = self.choose_action()
        new_event =  { 
            "action": new_robot_action,
            "change_activity": False
        }
        # update the counters
        self.store["noRobots"] += 1
        self.reducer_noRobots[new_robot_action] += 1

        # end condition
        if self.store["noRobots"] == MAX_NO_ROBOTS:
            self.end_game()

        # if the event can be done again, schedule it in the timeline
        if self.can_be_done(new_event):
            self.dispatch(new_event) 
        # else put it in the waiting queue
        else:
            self.events_hanging.append(new_event)
       
    # function for choosing an activity for a robot based on the pipeline's needs
    def choose_action(self):
        for action in ACTIONS:
            if self.reducer_noRobots[action]/self.store["noRobots"] < MAX_PROCENTAGES[action]:
                return action

    def end_game(self):
        print("\nGAME OVER \nIt took about {} seconds\n".format(self.time_now))
        print("Materials remained:\n{}".format(self.store))
        print("Final pipeline configuration:\n{}".format(self.reducer_noRobots))
        if DEBUG:
            print("Configuration:\n{}".format(MAX_PROCENTAGES))
        exit()


if __name__ == "__main__":
    Pipeline()   